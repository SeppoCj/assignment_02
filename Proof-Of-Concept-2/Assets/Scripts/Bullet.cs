﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform m_transform;
    private Vector3 m_spawnPoint;
    private Rigidbody2D m_rb;

    protected float directionAngle;
    protected float speed;
    


    protected virtual void Move()
    {
    
        Vector3 currentPos = new Vector3(transform.position.x + Mathf.Cos(Mathf.Deg2Rad * directionAngle), transform.position.y + Mathf.Sin(Mathf.Deg2Rad * directionAngle) * speed * Time.deltaTime, 0);
        m_rb.MovePosition(currentPos);
    }

   /* protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    } */

    private void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_transform = transform;
        m_spawnPoint = transform.position;
        SetMovement();
    }

    private void Update()
    {
        Move();

        if (transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }

    }

    protected virtual void SetMovement()
    {

    }


    // public void SpawnBullet(Vector3 spawnLocation, GameObject projectile, Quaternion startRotation) {
    // GameObject temp = Instantiate(projectile, spawnLocation, startRotation);



}

