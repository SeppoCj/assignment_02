﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float m_speed;
    public float m_slowDownFactor;

    private Rigidbody2D m_rb;
    public float cooldown = 0;
    Vector2 m_moveVector;

    BulletSpawner bs;
    CameraFollow cf;

    private void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        bs = FindObjectOfType<BulletSpawner>();
        cf = FindObjectOfType<CameraFollow>();
    }
    private void Update()
    {
        m_moveVector.x = Input.GetAxisRaw("Horizontal");
        m_moveVector.y = Input.GetAxisRaw("Vertical");

        
        cooldown -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.LeftControl) && cooldown <= 0)
        {

            cooldown = 5;
            StartCoroutine(slowZoom());
        }
    }
    private void FixedUpdate()
    {
        m_rb.MovePosition(m_rb.position + m_moveVector * m_speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(this.gameObject);

        }
    }

    public IEnumerator slowZoom()
    {
        m_speed *= m_slowDownFactor;
        Time.timeScale = 1 / m_slowDownFactor;
        cf.FollowPlayer(this.gameObject);
        yield return new WaitForSecondsRealtime(3f);
        cf.FollowPlayer(this.gameObject);
        Time.timeScale = 1;
        m_speed /= m_slowDownFactor;
    }
}
