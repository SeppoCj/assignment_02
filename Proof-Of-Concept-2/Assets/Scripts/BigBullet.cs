﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBullet : Bullet
{
    protected override void SetMovement()
    {
        directionAngle = Random.Range(-110,-70);
        speed = 2;
        transform.localScale = this.transform.localScale * 3;
    }
}
