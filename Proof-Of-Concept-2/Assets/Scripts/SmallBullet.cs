﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBullet : Bullet
{
    protected override void SetMovement() {
        directionAngle = Random.Range(-110, -70);
        speed = 5;
    }
}
