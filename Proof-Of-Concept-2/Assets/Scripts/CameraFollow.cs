﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public bool followPlayer = false;
    private GameObject player;

    private void Update()
    {
        if (followPlayer)
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y+1, -3f);
        }
        else {
            transform.position = new Vector3(0f, 0f, -5f);
        }
    }

    public void FollowPlayer(GameObject target) {
        if (followPlayer == false)
        {
            followPlayer = true;
        }
        else {
            followPlayer = false;
        }
        player = target;
    }
}
