﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] private GameObject smallBullet;
    [SerializeField] private GameObject bigBullet;
    [SerializeField] private float spawnTimer;
    [SerializeField] private float spawnTimerBase;
    [SerializeField] private CameraFollow cf;




    private int randomNumber;
    private float spawnXlocation;

    public GameObject[] bullets;

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(spawnBullet());
        cf = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        spawnTimerBase = spawnTimer;
    }

    // Update is called once per frame
    void Update() {
      

        if (cf.followPlayer == true)
        {
            spawnTimer = spawnTimerBase * 2;
        }
        else {
            spawnTimer = spawnTimerBase;
        }
      
        if (GameObject.FindGameObjectWithTag("Player") == null && Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(0);
        }

    }

    public IEnumerator spawnBullet()
    {
        yield return new WaitForSecondsRealtime(spawnTimer);
        randomNumber = Random.Range(0, 21);
        if (randomNumber != 20)
        {
            spawnXlocation = Random.Range(-11f, 11f);
            Instantiate(smallBullet, new Vector2(spawnXlocation, 5), Quaternion.identity);
        }
        else
        {
            spawnXlocation = Random.Range(-11f, 11f);

            Instantiate(bigBullet, new Vector2(spawnXlocation, 5), Quaternion.identity);
        }
        StartCoroutine(spawnBullet());
    }

}
