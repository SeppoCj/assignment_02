﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathEssentials : MonoBehaviour
{
    public Vector3 VectorFromAngle(float degrees) {
        return new Vector3(Mathf.Cos(degrees), Mathf.Sin(degrees));
    }
}
